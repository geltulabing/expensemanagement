<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
     // 1 Expense belongsTo Category,
    public function category(){
		return $this->belongsTo('App\Category');
	}

    // 1 Expense belongsTo User,
	public function user(){
		return $this->belongsTo('App\User');
	}

	
}
