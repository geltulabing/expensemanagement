<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //1 Role hasMany User
    public function users(){
        return $this->hasMany('App\User');
    }
}
