<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          //  true or false
        // dd(Auth::user());
        if(Auth::user()->role_id != 1 ){
            return redirect()->back();
        }
        else
        {
            //go to admin pages
            return $next($request);
        }
       
    }
}
