<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Roles";
        $roles = Role::orderBy('rolename')->paginate(10);
        //2) display all products, including soft deleted products
        // $companies =Company::withTrashed()->get();

        //3) displays only soft deleted products
        // $companies = Company::onlyTrashed()->paginate(10);

        return view('roles.index', compact("title","roles"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request->txtrolename);
        //  dd($request->txtdescription);

         try{  //
            $this->validate($request, [
                'txtrolename' => 'required',
                'txtdescription' => 'required',
            ]);
            
            $role = new Role;
            $role->rolename = $request->input('txtrolename');
            $role->description = $request->input('txtdescription');
            $role->save();
            return redirect("roles")->with('success', 'New role was successfully saved!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in saving new role.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);

        try{  
            $this->validate($request, [
                'txtrole' => 'required',
                'txtdesc' => 'required',
            ]);
            
            $role = Role::find($id);
            $role->rolename = $request->input('txtrole');
            $role->description = $request->input('txtdesc');
            $role->save();
            return redirect("roles")->with('success', 'Role has been successfully updated!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in updating role.");
        }
    }

    /** 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        $rolename = $role->rolename;
        $role->delete();

        return redirect()->back()->with("success","$rolename has been deleted!");
    }
}
