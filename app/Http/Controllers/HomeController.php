<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\ExpenseCategoryChart;
use App\Expense;
use App\Category;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
              
        if(Auth::user()->role_id == 1){
            $expensetotal = DB::table('expenses')
                            ->select('category_id', DB::raw('SUM(amount) as Total'))
                            ->groupBy('category_id');

            $totalpercategory = DB::table('categories')
                                ->joinSub($expensetotal, 'expenses', function ($join) {
                                    $join->on('categories.id', '=', 'expenses.category_id')
                                    ->orderBy('expenses.categoryname');
                                })->get();
        }
        else {
            $expensetotal = DB::table('expenses')
                            ->select('category_id', DB::raw('SUM(amount) as Total'))
                            ->where('user_id',Auth::user()->id)
                            ->groupBy('category_id');

            $totalpercategory = DB::table('categories')
                            ->joinSub($expensetotal, 'expenses', function ($join) {
                                $join->on('categories.id', '=', 'expenses.category_id')
                                ->orderBy('expenses.categoryname');
                            })->get();

        }
      

        //  dd($totalpercategory);

        $expensecategorychart = $this->ExpenseCategoryChart();

        return view("home", compact("expensecategorychart","totalpercategory"));

    }


    // CHART FUNCTION

     // USB ACCESS CHART
     private function ExpenseCategoryChart()
     {
 
         // 1) DB QUERY
         if(Auth::user()->role_id == 1){
            // DB::table('expenses')
            //     ->select('category_id', DB::raw('SUM(amount) as Total'))
            //     ->groupBy('category_id')
            //     ->map(function($item){
            //         return count($item);
            //     });

            // dataArr.map(data => {
            //     sum += data;
            // });
            // let percentage = (value*100 / sum).toFixed(2)+"%";
            // return percentage;

            $data = Expense::all();
            // dd($data);
            $data = $data->groupBy('category_id')
                    ->map(function ($item) {
                   
                    return count($item);
                    
            });

         }
         else{
            $data = Expense::all()->where('user_id',Auth::user()->id);
            $data = $data->groupBy('category_id')
                    ->map(function ($item) {
                
                    return count($item);
                    
            });
         }
        
         // dd($assets);
 
         // 2) INSTANTIATE CHART
         $expensecategorychart = New ExpenseCategoryChart;
         // 3) ASSIGN LABELS
    
         // dd($labels);
         $categorylabels = [];
         foreach($data->keys() as $key)
         {
             $category = Category::find($key);
             $categorylabels[] = $category->categoryname;
         }

         $expensecategorychart->labels($categorylabels);
         // dd($labels);
         // 4) ASSIGN DATA SET
         // types: line, bar, horizontal, doughnut, pie
 
         $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(0,128,0,1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(165,42,42,1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];

        $fillColors = [
            "rgba(255, 99, 132, 0.8)",
            "rgba(0,128,0,0.8)",
            "rgba(255, 205, 86, 0.8)",
            "rgba(51,105,232, 0.8)",
            "rgba(165,42,42,0.8)",
            "rgba(34,198,246, 0.8)",
            "rgba(153, 102, 255, 0.8)",
            "rgba(255, 159, 64, 0.8)",
            "rgba(233,30,99, 0.8)",
            "rgba(205,220,57, 0.8)"

        ];
 
         $expensecategorychart->dataset("My dataset","doughnut", $data->values())
         ->dashed([10])
         ->fill(true)
         ->color($borderColors)
         ->backgroundColor($fillColors);
 
         // 5) CUSTOMIZE CHART
         $expensecategorychart->minimalist(true); //boolean values
         $expensecategorychart->displayLegend(true,$data->keys());
         $title = "Expenses";
         $font_size = 18;
         $color = "orange";
         $bold = true;
         $expensecategorychart->title($title,$font_size,$color,$bold);
       
         return $expensecategorychart;
     }
}
