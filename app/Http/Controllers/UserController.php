<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use Auth;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

     public function index()
    {
        //
        $title = "Users";
        $users = User::orderBy('firstname')->paginate(10);
        $roles = Role::all();

        return view('users.index', compact("title","users", "roles"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->txtfname);
        // dd($request->txtlname);
        // dd($request->txtemail);
        // dd($request->password);
        // dd($request->optrole);

        try{  //
            $this->validate($request, [
                'txtfname' => 'required',
                'txtlname' => 'required',
                'txtemail' => 'required',
                'txtpassword' => 'required',
                'optrole' => 'required',
            ]);
            
            $user = new User;
            $user->firstname = $request->input('txtfname');
            $user->lastname = $request->input('txtlname');
            $user->email = $request->input('txtemail');
            $user->password = Hash::make($request->input('txtpassword'));
            // dd(Hash::make($request->input('txtpassword')));
            $user->role_id = $request->input('optrole');
            $user->save();
            return redirect("users")->with('success', 'New user was successfully saved!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in saving new user.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $title = "Profile";
        $user = User::find($id);
        $roles = Role::all();
      
        return view('users.edit',compact("title","user","roles"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        try{  //
            $this->validate($request, [
                'txtfirstname' => 'required',
                'txtlastname' => 'required',
                'txtmail' => 'required',
                // 'txtpassword' => 'required',
                'optuserrole' => 'required',
            ]);
            
            $user = User::find($id);
            $user->firstname = $request->input('txtfirstname');
            $user->lastname = $request->input('txtlastname');
            $user->email = $request->input('txtmail');
            // $user->password = Hash::make($request->input('txtpword'));
            $user->role_id = $request->input('optuserrole');
            $user->save();

            if(Auth::user()->role_id === 1){
                return redirect("users")->with('success', 'User details has been successfully updated!');
            }
            else{
                // dd("test");
                return redirect("/")->with('success', 'User details has been successfully updated!');
            }
           
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in updating user details.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $username = $user->firstname;
        $user->delete();

        return redirect()->back()->with("success","$username has been successfully deleted!");
    }

    public function showChangePasswordForm(){
        // dd("test");
        return view("users.changepassword");
    }
}
