<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\Category;
use App\User;
use Auth;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id == 1)
        {
            $title = "Expenses";
            $expenses = Expense::orderBy('entrydate')->paginate(10);
            $categories = Category::orderBy('categoryname')->get();
            $users = User::orderBy('firstname');
        }
        else 
        {
            $title = "Expenses";
            $expenses = Expense::orderBy('entrydate')->where('user_id',Auth::user()->id)->paginate(10);
            $categories = Category::orderBy('categoryname')->get();
            $users = User::orderBy('firstname');

        }
        return view('expenses.index', compact("title","expenses", "categories", "users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{  
            $this->validate($request, [
                'optcategory' => 'required',
                'txtamount' => 'required',
                'entrydate' => 'required',
            ]);
            
            $expense = new Expense;
            $expense->category_id = $request->input('optcategory');
            $expense->amount = $request->input('txtamount');
            $expense->entrydate = $request->input('entrydate');
            $expense->user_id = Auth::user()->id;
           
            $expense->save();
            return redirect("expenses")->with('success', 'New expense was successfully saved!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in saving new expense.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{  
            $this->validate($request, [
                'optexpensecategory' => 'required',
                'txtexpenseamount' => 'required',
                'dtexpenseentrydate' => 'required',
            ]);
            
            $expense = Expense::find($id);
            $expense->category_id = $request->input('optexpensecategory');
            $expense->amount = $request->input('txtexpenseamount');
            $expense->entrydate = $request->input('dtexpenseentrydate');
            $expense->user_id = Auth::user()->id;
           
            $expense->save();
            return redirect("expenses")->with('success', 'Expense has been successfully updated!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in updating expense.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);

        $expense = Expense::find($id);

        // $expensecategory = $expense->category->categoryname;
        $expense->delete();

        return redirect()->back()->with("success","Expense has been deleted!");
    }
}
