<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $title = "Expense Categories";
        $categories = Category::orderBy('categoryname')->paginate(10);
        //2) display all products, including soft deleted products
        // $companies =Company::withTrashed()->get();

        //3) displays only soft deleted products
        // $companies = Company::onlyTrashed()->paginate(10);

        return view('categories.index', compact("title","categories"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{  //
            $this->validate($request, [
                'txtcategoryname' => 'required',
                'txtdescription' => 'required',
            ]);
            
            $category = new Category;
            $category->categoryname = $request->input('txtcategoryname');
            $category->description = $request->input('txtdescription');
            $category->save();
            return redirect("categories")->with('success', 'New category was successfully saved!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in saving new category.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{  
            $this->validate($request, [
                'txtcategory' => 'required',
                'txtdesc' => 'required',
            ]);
            
            $category = Category::find($id);
            $category->categoryname = $request->input('txtcategory');
            $category->description = $request->input('txtdesc');
            $category->save();
            return redirect("categories")->with('success', 'Category has been successfully updated!');
            
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Error in updating category.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);

        $categoryname = $category->categoryname;
        $category->delete();

        return redirect()->back()->with("success","$categoryname has been deleted!");
    }
}
