<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //1 Category hasMany Expense
    public function expenses(){
        return $this->hasMany('App\Expense');
    }

    // public function getexpenses(){
    //     return $this->hasMany('App\Expense','category_id')->selectRaw('categories.*,sum(amount) as total')->groupBy('category_id');
    
    // }

    // public function categories(){
	// 	return $this->hasMany('App\Expense','category_id')->selectRaw('expense.*,sum(amount) as total')->groupBy('category_id');
	// }
}
