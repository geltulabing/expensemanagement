<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();



Route::group(["middleware" => ["auth"]], function()
{
    //USER PAGES
    Route::get('/', 'HomeController@index');

     // EXPENSES
     Route::get("/expenses", "ExpenseController@index");
     Route::post("/expenses","ExpenseController@store");

    //  USERS
    Route::get("/users/{id}/edit", "UserController@edit");
    Route::put("/users/{id}", "UserController@update");

    Route::get('/users/changepassword','UserController@showChangePasswordForm');

    /* ========================ROUTES WITH IDS=============================*/

    // EXPENSES
    Route::put("/expenses/{id}", "ExpenseController@update");
    Route::delete("/expenses/{id}", "ExpenseController@destroy");
    
    // ADMIN ONLY PAGES
    Route::group(["middleware" => "isAdmin"], function()
    {
         // HOME/DASHBOARD
         Route::get('/home', 'HomeController@index')->name('home');

        // CATEGORIES
        Route::get("/categories", "CategoryController@index");
        Route::post("/categories","CategoryController@store");
       
        // ROLES
        Route::get("/roles", "RoleController@index");
        Route::post("/roles","RoleController@store");

        // USERS
        Route::get("/users", "UserController@index");
        Route::post("/users","UserController@store");


        /* ========================ROUTES WITH IDS=============================*/

        // CATEGORIES
        Route::put("/categories/{id}", "CategoryController@update");
        Route::delete("/categories/{id}","CategoryController@destroy");
      
        // ROLE
        Route::put("/roles/{id}", "RoleController@update");
        Route::delete("/roles/{id}","RoleController@destroy");
        
        // USER
        // Route::put("/users/{id}", "UserController@update");
        Route::delete("/users/{id}", "UserController@destroy");
        

    
        
    });


});
