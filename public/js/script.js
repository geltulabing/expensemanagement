// MODAL FORMS
$(document).ready(function(){
    $(".show-modal").click(function(){
        $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $('.date').datepicker({  

    format: 'mm-dd-yyyy'

    });  
});



//CREATE ELEMENT FUNCTION
function createNewElement(element, text){
    let newElement = document.createElement(element);
    newElement.textContent = text;

    return newElement;
}

//SET ATTRIBUTE FUNCTION
//element.setAttribute(attr,value)
function setNewAttribute(element, attr, value){

    return element.setAttribute(attr, value);
}


// GLOBAL VARIABLES

let edit_modal_form = document.querySelector('#edit_modal_form');
let delete_modal_form =  document.querySelector('#delete_modal_form');

// ==========================================================================
// TABLE ROWS
document.addEventListener("DOMContentLoaded",()=>{

    const rows = document.querySelectorAll ("tr[data-href]");
    // console.log(rows);

    // ROLE
    rows.forEach(row=>{
        row.addEventListener("click",()=>{
            
            // ====================== ROLE ROW =============================
            if (row.classList.contains("role_row"))
            {
                let role_id = row.dataset.id;
                // GET VALUE FROM TABLE ROW TO MODAL INPUT TEXTS
                document.querySelector("#txtrole").value = row.cells[1].innerText;
                document.querySelector("#txtdesc").value = row.cells[2].innerText;        

                // CLICKING UPDATE/EDIT BUTTON
               document.querySelector("#btnEdit").addEventListener("click",()=>{
                    // console.log(role_id);
                  
                    let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);
    
                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");
    
                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","PUT");
                    setNewAttribute(inputmethod,"type", "hidden");
    
                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);

                    modal_form.setAttribute("action",`/roles/${role_id}`);
          
                });
               
                // CLICKING DELETE BUTTON
                document.querySelector("#btnDelete").addEventListener("click",()=>{
                    // alert("delete");
                   let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);

                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");

                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","DELETE");
                    setNewAttribute(inputmethod,"type", "hidden");

                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);
                    
                    modal_form.setAttribute("action", `/roles/${role_id}`);
                    
                });

               
                
            }

            // ====================== CATEGORY ROW =============================
            if (row.classList.contains("category_row"))
            {

                let category_id = row.dataset.id;
                
                // GET VALUE FROM TABLE ROW TO MODAL INPUT TEXTS
                document.querySelector("#txtcategory").value = row.cells[1].innerText;
                document.querySelector("#txtdesc").value = row.cells[2].innerText;
              
                
               // CLICKING UPDATE/EDIT BUTTON
               document.querySelector("#btnEdit").addEventListener("click",()=>{
                                    
                    let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);

                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");

                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","PUT");
                    setNewAttribute(inputmethod,"type", "hidden");

                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);

                    modal_form.setAttribute("action",`/categories/${category_id}`);
        
                });
            
                // CLICKING DELETE BUTTON
                document.querySelector("#btnDelete").addEventListener("click",()=>{
                    // alert("delete");
                    let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);

                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");

                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","DELETE");
                    setNewAttribute(inputmethod,"type", "hidden");

                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);
                    
                    modal_form.setAttribute("action", `/categories/${category_id}`);
                    
                });

           
                
            }

            // ====================== EXPENSE ROW===============================
            if (row.classList.contains("expense_row"))
            {
                // alert("hello");
                let expense_id = row.dataset.id;
            
                Array.from(document.querySelector("#optexpensecategory").options).forEach(function(option_element) {
                    let option_text = option_element.text;
                    let option_value = option_element.value;
                    // let is_option_selected = option_element.selected;
                    // console.log(option_text + option_value);
                    // console.log(row.cells[1].innerText);
                
                   if (option_text === row.cells[1].innerText){
                        let categoryselection = document.querySelector("#optexpensecategory");
                        categoryselection.value = option_value;
                   }

                });
                
                let expenseamount = row.cells[2].innerText;     
                // console.log(expenseamount);
                let currencyconverted = Number(expenseamount.replace(/[^0-9\.-]+/g,""));
                
                document.querySelector("#txtexpenseamount").value = currencyconverted;

                document.querySelector("#dtexpenseentrydate").value = row.cells[3].innerText;


                // CLICKING UPDATE/EDIT BUTTON
               document.querySelector("#btnEdit").addEventListener("click",()=>{
            
                    let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);

                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");

                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","PUT");
                    setNewAttribute(inputmethod,"type", "hidden");

                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);

                    modal_form.setAttribute("action",`/expenses/${expense_id}`);
        
                });
            
                // CLICKING DELETE BUTTON
                document.querySelector("#btnDelete").addEventListener("click",()=>{
                    // alert("delete");
                let modal_form = document.querySelector('#modal_form');

                    let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    // console.log(token);

                    let inputtoken = createNewElement("input","");
                    setNewAttribute(inputtoken,"name","_token");
                    setNewAttribute(inputtoken,"value",token);
                    setNewAttribute(inputtoken,"type", "hidden");

                    let inputmethod = createNewElement("input","");
                    setNewAttribute(inputmethod,"name","_method");
                    setNewAttribute(inputmethod,"value","DELETE");
                    setNewAttribute(inputmethod,"type", "hidden");

                    modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                    modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);
                    
                    modal_form.setAttribute("action", `/expenses/${expense_id}`);
                    
                });

           
                
            }

             // ====================== USER ROW =============================
             if (row.classList.contains("user_row"))
             {
                 let user_id = row.dataset.id;
                 // GET VALUE FROM TABLE ROW TO MODAL INPUT TEXTS
                 document.querySelector("#txtfirstname").value = row.cells[1].innerText;
                 document.querySelector("#txtlastname").value = row.cells[2].innerText;        
                 document.querySelector("#txtmail").value = row.cells[3].innerText;        
                //  document.querySelector("#txtpword").value = row.cells[4].innerText;        

                 Array.from(document.querySelector("#optuserrole").options).forEach(function(option_element) {
                    let option_text = option_element.text;
                    let option_value = option_element.value;
                    
                   if (option_text === row.cells[4].innerText){
                        let userroleselection = document.querySelector("#optuserrole");
                        userroleselection.value = option_value;

                   }

                });

                if(row.cells[4].innerText === "Administrator"){
                    document.querySelector("#txtfirstname").disabled = true;
                    document.querySelector("#txtlastname").disabled = true;       
                    document.querySelector("#txtmail").disabled = true;
                    document.querySelector("#optuserrole").disabled = true;
                    document.querySelector("#btnEdit").disabled = true;
                    document.querySelector("#btnDelete").disabled = true;
                }
                else {

                    document.querySelector("#txtfirstname").disabled = false;
                    document.querySelector("#txtlastname").disabled = false;       
                    document.querySelector("#txtmail").disabled = false;
                    document.querySelector("#optuserrole").disabled = false;
                    document.querySelector("#btnEdit").disabled = false;
                    document.querySelector("#btnDelete").disabled = false;
                    // CLICKING UPDATE/EDIT BUTTON
                    document.querySelector("#btnEdit").addEventListener("click",()=>{
                                        
                        let modal_form = document.querySelector('#modal_form');

                        let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                    
                        let inputtoken = createNewElement("input","");
                        setNewAttribute(inputtoken,"name","_token");
                        setNewAttribute(inputtoken,"value",token);
                        setNewAttribute(inputtoken,"type", "hidden");
        
                        let inputmethod = createNewElement("input","");
                        setNewAttribute(inputmethod,"name","_method");
                        setNewAttribute(inputmethod,"value","PUT");
                        setNewAttribute(inputmethod,"type", "hidden");
        
                        modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                        modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);
                    
                    //  console.log(user_id);
                        modal_form.setAttribute("action",`/users/${user_id}`);
            
                    });
                
                    // CLICKING DELETE BUTTON
                    document.querySelector("#btnDelete").addEventListener("click",()=>{
                        // alert("delete");
                    let modal_form = document.querySelector('#modal_form');

                        let token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                        // console.log(token);

                        let inputtoken = createNewElement("input","");
                        setNewAttribute(inputtoken,"name","_token");
                        setNewAttribute(inputtoken,"value",token);
                        setNewAttribute(inputtoken,"type", "hidden");

                        let inputmethod = createNewElement("input","");
                        setNewAttribute(inputmethod,"name","_method");
                        setNewAttribute(inputmethod,"value","DELETE");
                        setNewAttribute(inputmethod,"type", "hidden");

                        modal_form.insertBefore(inputtoken, modal_form.childNodes[0]);
                        modal_form.insertBefore(inputmethod, modal_form.childNodes[1]);
                        
                        modal_form.setAttribute("action", `/users/${user_id}`);
                        
                    });

                }
               
                
             }

        });
    });
   
});
