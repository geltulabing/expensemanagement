@extends('layouts.app')
@section('content')
@include("partials.navbar")
@include("partials.alert")


<div class="container">
  <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h2>{{$title}}</h2>
            </div>
        </div>
    </div>
  </div>

  <!-- ADD BUTTON AND PAGINATION LINKS -->
  <div class="row my-5">
    <div class="col d-flex pr-0">
      <button type="button" class="btn btn-primary btn-md show-modal">  <i class="fas fa-plus-circle"></i>  Add New Expense</button>
    
      <div class="ml-auto">
          {{$expenses->links()}}   
      </div>
      
    </div>
  </div>

  <div class="row">
    <div class="col-12">
        <table id ="dataTable" class="table table-bordered table-striped  table-sm table-hover table-responsive-lg">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Expense Category</th>
              <th scope="col">Amount</th>
              <th scope="col">Entry Date</th>
              <th scope="col">Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach($expenses as $expense)
            <tr data-href="/expenses/{{ $expense->id }}"
                class="expense_row"
                data-toggle="modal" data-target="#EditDeleteModal" 
                data-id="{{$expense->id}}" data-name="{{$expense->category->categoryname}}"
            >
           
              <th scope="row">{{$loop->iteration}}</th>
              <td>{{$expense->category->categoryname}}</td>
              <td>@money_format($expense->amount)</td>
              <td>{{$expense->entrydate}}</td>
              <td>{{$expense->created_at}}</td>
            </tr>
            @endforeach  
          </tbody>
        </table>
    </div>
  </div>
</div>



<!-- ADD MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">New Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{action('ExpenseController@store')}}">
        {{@csrf_field()}}
        <div class="modal-body">
            <!-- CATEGORY -->
            <div class="form-group">
              <label for="optcategory" class="col-form-label">Expense Category:</label>
               <select class="form-control mb-3" name="optcategory" required>
                  <option class="form-control">Choose Expense Category</option>
                  @foreach($categories as $category)
                      <option class="form-control" value ="{{$category->id}}">{{$category->categoryname}}</option>
                  @endforeach
              </select>
            </div>
            <!-- AMOUNT -->
            <div class="form-group">
              <label for="txtamount" class="col-form-label">Amount:</label>
              <input type="number" step="0.01" class="form-control" id="txtamount" name="txtamount" required>
            </div>
            <!-- DATE -->
            <div class="form-group">
              <label for="entrydate" class="col-form-label">Entry Date:</label>
              <input type="date" class="form-control" id="entrydate" name="entrydate" required>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
          <button id="add_modal_form" class="btn btn-primary d-inline"> <i class="fas fa-save"></i> Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- EDIT DELETE MODAL -->
<div class="modal fade" id="EditDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Update or Delete Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form method="POST" id="modal_form">
            <div class="modal-body">
              <!-- CATEGORY -->
              <div class="form-group">
                <label for="optexpensecategory" class="col-form-label">Expense Category:</label>
                <select class="form-control mb-3" name="optexpensecategory" id="optexpensecategory">
                    <option class="form-control">Choose Expense Category</option>
                    @foreach($categories as $category)
                        <option class="form-control" value ="{{$category->id}}"
                          {{ $category->id == $expense->category_id ? 
                                              "selected" : ""}}>
                            {{$category->categoryname}}
                        </option>
                    @endforeach
                </select>
              </div>
              <!-- AMOUNT -->
              <div class="form-group">
                <label for="txtexpenseamount" class="col-form-label">Amount:</label>
                <input type="number" step="0.01" class="form-control" id="txtexpenseamount" name="txtexpenseamount">
              </div>
              <!-- DATE -->
              <div class="form-group">
                <label for="dtexpenseentrydate" class="col-form-label">Entry Date:</label>
                <input type="date" class="form-control" id="dtexpenseentrydate" name="dtexpenseentrydate">
              </div>

          </div>
          <div class="modal-footer">
            <div class="col d-flex pr-0">

                  <button id="btnDelete" class="btn btn-danger d-inline"><i class="fas fa-trash-alt"></i> Delete</button>
         
                <div class="ml-auto">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
                  <button id="btnEdit" class="btn btn-primary d-inline"><i class="fas fa-pencil-alt"></i> Update</button>
                </div>
                
            </div>
          </div>
        </form>
    </div>
  </div>
</div>
@endsection