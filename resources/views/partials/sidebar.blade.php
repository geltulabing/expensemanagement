
    <!-- Sidebar -->
    <nav id="sidebar">
        <div id="header-display" class="sidebar-header border-bottom border-white">
                <h4 class="text-center"><i class="fas fa-user mx-2"></i></h4>
                <p class="text-center text-white"> Hello, {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
        </div>
            <ul class="list-unstyled components">
                <!-- ADMIN MENUS -->
                @if(Auth::check() && Auth::user()->role_id == 1)

               <!-- HOME/DASHBOARD -->
                <li><a href="/home"><i class="fas fa-home mx-2"></i>Dashboard</a></li>
                
                <!-- USER MANAGEMENT -->
                <li>
                    <a href="#setupSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle menuItem">
                    <i class="fas fa-cogs mx-2"></i>User Management</a>
                    <ul class="collapse list-unstyled" id="setupSubmenu">
                         <!-- ROLES -->
                        <li><a href="/roles"><i class="fas fa-user-tag"></i>  Roles</a></li>
                        <!-- USERS -->
                        <li><a href="/users"><i class="fas fa-users"></i>  Users</a></li>
                    </ul>
                </li>

                <!-- EXPENSE MANAGEMENT -->
                <li>
                    <a href="#assetSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle menuItem">
                    <i class="fas fa-clipboard-list mx-2"></i> Expense Management</a>
                    <ul class="collapse list-unstyled" id="assetSubmenu">
                        <!-- CATEGORIES -->
                        <li><a href="/categories"><i class="fas fa-list-alt fa-1x mx-2"></i>Expense Categories</a></li>
                        <!-- EXPENSES -->
                        <li><a href="/expenses"><i class="fas fa-edit fa-1x mx-2"></i>Expenses</a></li>
                    </ul>
                </li>
                @endif


                <!-- USER MENUS -->
                @if(Auth::check() && Auth::user()->role_id != 1)
              
                  <ul class="list-unstyled" >
                      
                        <li><a href="/"><i class="fas fa-home mx-2"></i> Dashboard</a></li>
                        <li><a href="/users/{{Auth::user()->id}}/edit"><i class="fas fa-id-badge mx-2"></i> Profile</a></li>
                        <li><a href="#"><i class="fas fa-lock mx-2"></i> Change Password</a></li>
                        <li><a href="/expenses"><i class="fas fa-sticky-note fa-1x mx-2"></i> Expenses</a></li>
                    </ul>
               
                @endif
            </ul>
    
        </div>
    </nav>

      