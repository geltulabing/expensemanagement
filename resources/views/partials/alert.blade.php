<div id="col-12 my-2">
	<div class="alert-container">
		@if(Session::has("success"))
		<div class="alert alert-success col-12" id="alert_success">
		<button type="button" class="close" data-dismiss="alert">×</button>	
			{{ Session::get('success') }}
			<!-- SOFT DELETE - UNDO DELETION -->
			@if(Session::has("undo_url"))
				<a href="{{Session::get('undo_url')}}"
				class="btn btn-link font-weight-bold">
					Undo
				</a>
			@endif
		</div>
		@elseif(Session::has("error"))
		<div class="alert alert-danger col-12" id="alert_error">
		<button type="button" class="close" data-dismiss="alert">×</button>	
			{{ Session::get('error') }}
		</div>
		@elseif(Session::has("info"))
		<div class="alert alert-info col-12" id="alert_info">
		<button type="button" class="close" data-dismiss="alert">×</button>	
			{{ Session::get('info') }}
		</div>
		@endif
	</div>
</div>

