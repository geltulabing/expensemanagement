@extends('layouts.app')
@section('content')
@include("partials.navbar")
@include("partials.alert")

<div class="container">
  <div class="row">
    <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h2>{{$title}}</h2>
              </div>
          </div>
      </div>
    </div>


  <!-- ADD BUTTON AND PAGINATION LINKS -->
  <div class="row my-5">
    <div class="col d-flex pr-0">
      <button type="button" class="btn btn-primary btn-md show-modal">  <i class="fas fa-plus-circle"></i>  Add New Role</button>
    
      <div class="ml-auto">
          {{$roles->links()}}   
      </div>
      
    </div>
  </div>

  
  <!-- TABLE -->
  <div class="row">
    <div class="col-12">
        <table id="dataTable" class="display table table-bordered table-striped  table-sm table-hover table-responsive-lg">
          <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Role Name</th>
                <th scope="col">Description</th>
                <th scope="col">Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach($roles as $role)
            <tr data-href="/roles/{{ $role->id }}"
                class="role_row"
                data-toggle="modal" data-target="#EditDeleteModal" 
                data-id="{{$role->id}}" data-name="{{$role->rolename}}"
            >
           
                <th scope="row">{{$loop->iteration}}</th>  
                <td>{{$role->rolename}}</td>
                <td>{{$role->description}}</td>
                <td>{{$role->created_at}}</td>
            </tr>
            @endforeach  
          </tbody>
        </table>
    </div>
  </div>
</div>


<!-- ADD MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">New Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{action('RoleController@store')}}">
        {{@csrf_field()}}
        <div class="modal-body">
            <div class="form-group">
              <label for="txtrolename" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="txtrolename" name="txtrolename" required>
            </div>
            <div class="form-group">
              <label for="txtdescription" class="col-form-label">Description:</label>
              <textarea class="form-control" id="txtdescription" name="txtdescription" required></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
          <button id="add_modal_form" class="btn btn-primary d-inline"> <i class="fas fa-save"></i> Save</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- EDIT DELETE MODAL -->
<div class="modal fade" id="EditDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Update or Delete Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" id="modal_form">
        <div class="modal-body">
            <div class="form-group">
              <label for="txtrole" class="col-form-label">Role Name:</label>
              <input type="text" class="form-control" id="txtrole" name="txtrole">
            </div>
            <div class="form-group">
              <label for="txtdesc" class="col-form-label">Description:</label>
              <textarea class="form-control" id="txtdesc" name="txtdesc"></textarea>
            </div>
        </div>
        <div class="modal-footer">
          <div class="col d-flex pr-0">
                <button id="btnDelete" class="btn btn-danger d-inline"><i class="fas fa-trash-alt"></i> Delete</button>
              <div class="ml-auto">
                
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
        
                <button id="btnEdit" class="btn btn-primary d-inline"><i class="fas fa-pencil-alt"></i> Update</button>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection