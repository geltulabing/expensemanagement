@extends('layouts.app')
@section('content')
@include("partials.navbar")
@include('partials.alert')
<div class="container">
    <div class="row">
        <div class="col-12 mb-3">
            <div class="card">
                <div class="card-body">
                    <h2>{{$title}}</h2>
                </div>
            </div>
        </div>
    </div>
    <form action="/users/{{$user->id}}" method="POST">
       @csrf
       {{method_field("PUT")}}
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group">
                            <!-- FIRSTNAME -->
                            <div class="form-group">
                                <label for="txtfirstname" class="col-form-label">Firstname:</label>
                                <input type="text" class="form-control" id="txtfirstname" name="txtfirstname" value="{{$user->firstname}}">
                            </div>
                            <!-- LASTNAME -->
                            <div class="form-group">
                                <label for="txtlastname" class="col-form-label">Lastname:</label>
                                <input type="text" class="form-control" id="txtlastname" name="txtlastname" value="{{$user->lastname}}">
                            </div>
                            <!-- EMAIL -->
                            <div class="form-group">
                                <label for="txtmail" class="col-form-label">Email:</label>
                                <input type="email" class="form-control" id="txtmail" name="txtmail" value="{{$user->email}}">
                            </div>
                          
                            <!-- ROLE -->
                            <div>
                            <label for="optuserrole" class="col-form-label">Role:</label>
                            <select class="form-control mb-3" name="optuserrole" id="optuserrole">
                                <option class="form-control">Choose Role</option>
                                @foreach($roles as $role)
                                    <option class="form-control" value ="{{$role->id}}"
                                            {{ $role->id == $user->role_id ? 
                                              "selected" : ""}}
                                    >
                                        {{$role->rolename}}
                                    </option>
                                @endforeach
                            </select>
                            </div>

                        </div>
                    </div>

                    <div class="card-footer">                 
                        <button class="btn btn-primary form-control">
                            <i class="fas fa-save"></i> Save Changes
                        </button>
                    </div>
                </div>
            </div>          
        </div>

        
    </form>
</div>
@endsection