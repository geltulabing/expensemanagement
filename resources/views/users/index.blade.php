@extends('layouts.app')
@section('content')
@include("partials.navbar")
@include("partials.alert")


<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h2>{{$title}}</h2>
          </div>
      </div>
    </div>
  </div>


  <div class="row">
    <!-- BUTTONS -->
    <div class="col d-flex pr-0">
      <div>
        <button type="button" class="btn btn-primary show-modal my-5">
        <i class="fas fa-plus-circle"></i>  Add New User
        </button>
      </div>
        
      <div class="ml-auto">
          {{$users->links()}}   
      </div>
      
    </div>
  </div>
  <div class="row">
    <div class="col-12">
        <table id="dataTable" class="table table-bordered table-striped  table-sm table-hover table-responsive-lg">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Firstname</th>
              <th scope="col">Lastname</th>
              <th scope="col">Email</th>
              <th scope="col">Role</th>
              <th scope="col">Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr data-href="/users/{{ $user->id }}"
                class="user_row"
                data-toggle="modal" data-target="#EditDeleteModal" 
                data-id="{{$user->id}}" data-name="{{$user->firstname}}"
            >
           
              <th scope="row">{{$loop->iteration}}</th> 
              <td>{{$user->firstname}}</td>
              <td>{{$user->lastname}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->role->rolename}}</td>
              <td>{{$user->created_at}}</td>
            </tr>
            @endforeach  
          </tbody>
        </table>
    </div>
  </div>
</div>

<!-- ADD MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">New User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{action('UserController@store')}}">
        {{@csrf_field()}}
        <div class="modal-body">
            <!-- FIRSTNAME -->
            <div class="form-group">
              <label for="txtfname" class="col-form-label">Firstname:</label>
              <input type="text" class="form-control" id="txtfname" name="txtfname">
            </div>
            <!-- LASTNAME -->
            <div class="form-group">
              <label for="txtlname" class="col-form-label">Lastname:</label>
              <input type="text" class="form-control" id="txtlname" name="txtlname">
            </div>
            <!-- EMAIL -->
            <div class="form-group">
              <label for="txtemail" class="col-form-label">Email:</label>
              <input type="email" class="form-control" id="txtemail" name="txtemail">
            </div>
            <!-- PASSWORD -->
            <div class="form-group">
              <label for="txtpassword" class="col-form-label">Password:</label>
              <input type="password" class="form-control" id="txtpassword" name="txtpassword">
            </div>
            <!-- ROLE -->
            <div>
              <label for="txtrole" class="col-form-label">Role:</label>
               <select class="form-control mb-3" name="optrole">
                  <option class="form-control">Choose Role</option>
                  @foreach($roles as $role)
                      <option class="form-control" value ="{{$role->id}}">{{$role->rolename}}</option>
                  @endforeach
              </select>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
          <button id="add_modal_form" class="btn btn-primary d-inline"> <i class="fas fa-save"></i> Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- EDIT / DELETE MODAL -->
<div class="modal fade" id="EditDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Edit or Delete User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" id="modal_form">
        <div class="modal-body">
            <!-- FIRSTNAME -->
            <div class="form-group">
              <label for="txtfirstname" class="col-form-label">Firstname:</label>
              <input type="text" class="form-control" id="txtfirstname" name="txtfirstname">
            </div>
            <!-- LASTNAME -->
            <div class="form-group">
              <label for="txtlastname" class="col-form-label">Lastname:</label>
              <input type="text" class="form-control" id="txtlastname" name="txtlastname" required>
            </div>
            <!-- EMAIL -->
            <div class="form-group">
              <label for="txtmail" class="col-form-label">Email:</label>
              <input type="email" class="form-control" id="txtmail" name="txtmail" required>
            </div>
            <!-- ROLE -->
            <div>
              <label for="optuserrole" class="col-form-label">Role:</label>
               <select class="form-control mb-3" name="optuserrole" id="optuserrole" required>
                  <option class="form-control">Choose Role</option>
                  @foreach($roles as $role)
                      <option class="form-control" value ="{{$role->id}}">{{$role->rolename}}</option>
                  @endforeach
              </select>
            </div>

        </div>
        <div class="modal-footer">
          <div class="col d-flex pr-0">
                <button id="btnDelete" class="btn btn-danger d-inline"> <i class="fas fa-trash-alt"></i> Delete</button>
              <div class="ml-auto">
                
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancel</button>
        
                <button id="btnEdit" class="btn btn-primary d-inline"><i class="fas fa-pencil-alt"></i> Update</button>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection