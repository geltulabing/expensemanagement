@extends('layouts.app')
@section('content')
@include("partials.navbar")


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h2>Dashboard</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="dashboard-header my-3">
        <div class="container-fluid">
            <div class="row">
                <!-- TABLE - EXPENSE CATEGORY -->
                <div class="chart col-5">
                    <div class="bg-white d-flex align-items-center justify-content-center shadow p-3 mb-3 border rounded">

                        <table class="table table-bordered table-striped  table-sm table-hover table-responsive-lg">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Expense Category</th>
                                    <th scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($totalpercategory as $totalcategory)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$totalcategory->categoryname}}</td>
                                        <td>@money_format($totalcategory->Total)</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <!-- PIE CHART - EXPENSE CATEGORY -->
                <div class="chart col-7">
                    <div class="bar-chart shadow bg-white p-3 mb-3 border rounded">
                        <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div>
                    
                    <div>
                        {!! $expensecategorychart->container() !!}
                    </div>
                    <!-- Numbers-->
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")
    {!! $expensecategorychart->script() !!}
   
@endsection
