<?php

use Illuminate\Database\Seeder;

class ExpensesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('expenses')->delete();
        
        \DB::table('expenses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'amount' => '1000.750',
                'entrydate' => '2020-03-02',
                'category_id' => 1,
                'user_id' => 2,
                'created_at' => '2020-03-07 15:37:54',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'amount' => '500.000',
                'entrydate' => '2020-03-03',
                'category_id' => 4,
                'user_id' => 1,
                'created_at' => '2020-03-07 15:37:54',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'amount' => '1500.750',
                'entrydate' => '2020-03-04',
                'category_id' => 2,
                'user_id' => 2,
                'created_at' => '2020-03-07 15:37:54',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 16,
                'amount' => '2500.000',
                'entrydate' => '2020-03-07',
                'category_id' => 6,
                'user_id' => 2,
                'created_at' => '2020-03-07 15:38:33',
                'updated_at' => '2020-03-08 14:54:27',
            ),
            4 => 
            array (
                'id' => 17,
                'amount' => '7500.000',
                'entrydate' => '2020-03-07',
                'category_id' => 10,
                'user_id' => 1,
                'created_at' => '2020-03-07 15:43:51',
                'updated_at' => '2020-03-08 14:51:17',
            ),
            5 => 
            array (
                'id' => 18,
                'amount' => '1000.000',
                'entrydate' => '2020-03-08',
                'category_id' => 8,
                'user_id' => 1,
                'created_at' => '2020-03-08 14:12:37',
                'updated_at' => '2020-03-08 14:12:37',
            ),
            6 => 
            array (
                'id' => 20,
                'amount' => '1000.000',
                'entrydate' => '2020-03-07',
                'category_id' => 1,
                'user_id' => 2,
                'created_at' => '2020-03-08 14:54:06',
                'updated_at' => '2020-03-08 14:54:06',
            ),
            7 => 
            array (
                'id' => 21,
                'amount' => '1500.000',
                'entrydate' => '2020-03-06',
                'category_id' => 6,
                'user_id' => 1,
                'created_at' => '2020-03-08 18:51:54',
                'updated_at' => '2020-03-08 18:51:54',
            ),
        ));
        
        
    }
}