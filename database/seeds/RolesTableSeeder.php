<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'rolename' => 'Administrator',
                'description' => 'Administrator / Super User',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'rolename' => 'User',
                'description' => 'User access',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'rolename' => 'Role',
                'description' => 'Role description',
                'created_at' => '2020-03-06 16:42:14',
                'updated_at' => '2020-03-06 16:42:14',
            ),
            3 => 
            array (
                'id' => 6,
                'rolename' => 'Role Role',
                'description' => 'Role Role Role',
                'created_at' => '2020-03-07 12:04:03',
                'updated_at' => '2020-03-07 12:04:03',
            ),
        ));
        
        
    }
}