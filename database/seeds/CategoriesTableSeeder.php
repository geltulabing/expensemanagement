<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'categoryname' => 'Travel',
                'description' => 'Travel Expenses',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'categoryname' => 'Miscellaneous',
                'description' => 'Miscellaneous Expenses',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'categoryname' => 'Utilities',
                'description' => 'Utilities Expenses',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'categoryname' => 'Salaries and Wages',
                'description' => 'Salaries and Wages Expenses',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'categoryname' => 'Food',
                'description' => 'Food Expenses',
                'created_at' => '2020-03-06 11:15:08',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'categoryname' => 'Rent',
                'description' => 'Rental Expense',
                'created_at' => '2020-03-06 16:56:37',
                'updated_at' => '2020-03-06 16:56:37',
            ),
            6 => 
            array (
                'id' => 8,
                'categoryname' => 'Insurance',
                'description' => 'Insurance Expense',
                'created_at' => '2020-03-06 16:58:49',
                'updated_at' => '2020-03-06 16:58:49',
            ),
            7 => 
            array (
                'id' => 9,
                'categoryname' => 'Advertising',
                'description' => 'Advertising Expense',
                'created_at' => '2020-03-06 16:59:12',
                'updated_at' => '2020-03-06 16:59:12',
            ),
            8 => 
            array (
                'id' => 10,
                'categoryname' => 'Marketing',
                'description' => 'Marketing Expense',
                'created_at' => '2020-03-06 17:00:58',
                'updated_at' => '2020-03-06 17:00:58',
            ),
            9 => 
            array (
                'id' => 11,
                'categoryname' => 'Interest',
                'description' => 'Interest Expense',
                'created_at' => '2020-03-06 17:01:32',
                'updated_at' => '2020-03-06 17:01:32',
            ),
            10 => 
            array (
                'id' => 17,
                'categoryname' => 'Sample',
                'description' => 'Sample Expense Category',
                'created_at' => '2020-03-07 12:27:14',
                'updated_at' => '2020-03-07 12:27:14',
            ),
        ));
        
        
    }
}