<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'firstname' => 'Gelyn',
                'lastname' => 'Tulabing',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$2zRBe5Kx.N4iU2zkR3Wm5eUeMwvw4Fy4M1rs6YpiBjwr6YNXCNKoW',
                'remember_token' => NULL,
                'created_at' => '2020-03-07 15:34:51',
                'updated_at' => NULL,
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'firstname' => 'User abc',
                'lastname' => 'User 123',
                'email' => 'user@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JCWRupOHgTAFYWtYZZHNV..8HcabSrxKpcI9ZJuVOrEUKq2LdtWP2',
                'remember_token' => NULL,
                'created_at' => '2020-03-07 15:34:51',
                'updated_at' => '2020-03-08 18:02:55',
                'role_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'firstname' => 'sample',
                'lastname' => 'samples',
                'email' => 'sample@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JCWRupOHgTAFYWtYZZHNV..8HcabSrxKpcI9ZJuVOrEUKq2LdtWP2',
                'remember_token' => NULL,
                'created_at' => '2020-03-07 15:34:51',
                'updated_at' => '2020-03-08 18:01:53',
                'role_id' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'firstname' => 'User2',
                'lastname' => 'User2',
                'email' => 'user2@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JCWRupOHgTAFYWtYZZHNV..8HcabSrxKpcI9ZJuVOrEUKq2LdtWP2',
                'remember_token' => NULL,
                'created_at' => '2020-03-07 15:34:51',
                'updated_at' => NULL,
                'role_id' => 2,
            ),
            4 => 
            array (
                'id' => 6,
                'firstname' => 'User4',
                'lastname' => 'User4',
                'email' => 'user4@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JCWRupOHgTAFYWtYZZHNV..8HcabSrxKpcI9ZJuVOrEUKq2LdtWP2',
                'remember_token' => NULL,
                'created_at' => '2020-03-07 15:34:51',
                'updated_at' => NULL,
                'role_id' => 2,
            ),
            5 => 
            array (
                'id' => 8,
                'firstname' => 'Purple',
                'lastname' => 'Bug',
                'email' => 'purplebug@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$EhrUjGvnUysTgNiCyFTGy.QjkXDyZ4UuT2QfYdlAuRK8GvqWpL.KS',
                'remember_token' => NULL,
                'created_at' => '2020-03-08 15:49:21',
                'updated_at' => '2020-03-08 15:49:21',
                'role_id' => 1,
            ),
            6 => 
            array (
                'id' => 9,
                'firstname' => 'test',
                'lastname' => 'test',
                'email' => 'test@email.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$SvfUALG0cGnvtnPOTBhda.L7mYNhVnr84D/YBj47mirIWmbsR3oxy',
                'remember_token' => NULL,
                'created_at' => '2020-03-08 15:53:41',
                'updated_at' => '2020-03-08 18:02:36',
                'role_id' => 2,
            ),
            7 => 
            array (
                'id' => 10,
                'firstname' => 'abcabc',
                'lastname' => 'abcabc',
                'email' => 'abc@email.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$A6TYRh.rQngkV7O6uDV/5eJE6WZ8zYkyBh2aC8tc7LilhktePibly',
                'remember_token' => NULL,
                'created_at' => '2020-03-08 16:06:30',
                'updated_at' => '2020-03-08 17:48:35',
                'role_id' => 2,
            ),
        ));
        
        
    }
}